// eslint-disable-next-line import/no-extraneous-dependencies
const postcssPresetEnv = require('postcss-preset-env');
// eslint-disable-next-line import/no-extraneous-dependencies
const purgeCss = require('@fullhuman/postcss-purgecss');
// eslint-disable-next-line import/no-extraneous-dependencies
const purgeCssFromHtml = require('purgecss-from-html');
const configLoader = require('./scripts/webpack/config-loader');
const { lookupPlaceholderMultiple } = require('./scripts/webpack/placeholder');
const {
  purgeCssFromTwig,
  purgeCssFromText,
  purgeCssFromJs
} = require('./scripts/webpack/purgecss');
// Load config
const { BROWSERS, PURGE_CSS, PURGE_CSS_SAFE_LIST } = configLoader();

// Lookup placeholders
const purgeCssPaths = lookupPlaceholderMultiple(PURGE_CSS) || false;

module.exports = ({ env }) => {
  // noinspection JSValidateTypes
  const config = {
    parser: 'postcss-scss',
    plugins: [postcssPresetEnv({ browsers: BROWSERS })]
  };

  if (purgeCssPaths && env !== 'none') {
    config.plugins.push(
      purgeCss({
        content: purgeCssPaths || [],
        safelist: PURGE_CSS_SAFE_LIST || [],
        extractors: [
          {
            extractor: purgeCssFromHtml,
            extensions: ['html']
          },
          {
            extractor: purgeCssFromTwig,
            extensions: ['twig']
          },
          {
            extractor: purgeCssFromJs,
            extensions: ['js']
          },
          {
            extractor: purgeCssFromText,
            extensions: ['php', 'inc', 'theme', 'module']
          }
        ]
      })
    );
  }

  return config;
};
