[![Logo](https://www.codev-it.at/logo.png)](https://www.codev-it.at)

# Codev-IT HTML Boilerplate

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://bitbucket.org/codev-it/html-boilerplate/src/master/LICENSE)

## Description

The Codev-IT HTML Boilerplate is a robust starting point for web development
projects, integrating Webpack and React. This setup is designed to streamline
the development process, offering a solid foundation for building sophisticated
web applications.

## Features

* **Webpack Integration**: Simplifies bundling of assets and modules.
* **React Support**: Ready-to-use setup for React applications.
* **Development and Production Builds**: Custom scripts for different build
  environments.
* **Extensive Dependency Support**: Includes a wide range of development and
  production dependencies.
* **Babel Configuration**: Pre-configured to work with the latest JavaScript
  features.
* **ESLint and Prettier**: Ensures code quality and consistent formatting.

## Getting Started

Clone the repository and navigate to the project directory:

```bash
git clone https://bitbucket.org/codev-it/html-boilerplate.git
cd codev-it_html-boilerplate
```

## Usage

This boilerplate comes with several npm scripts to facilitate development and
production workflows:

### Development

```npm run build```
Compiles the project for development. It includes detailed stats and progress
indicators.

```npm run watch```
Automatically recompiles your project when files change. Keeps webpack in watch
mode.

```npm run server```
Starts a development server with webpack-dev-server, enabling features like live
reloading.

### Production

```npm run build-prod```
Compiles the project for production. This script optimizes the build for
performance and reduces file sizes.

```npm run watch-prod```
Watches your files for changes and recompiles them in production mode.

```npm run server-prod```
Launches a server with webpack-dev-server optimized for production environments.

### Mode-Neutral

```npm run build-none```
Executes a webpack build without any predefined mode. This is useful for testing
different webpack configurations.

```npm run watch-none```
Keeps webpack in watch mode without any specific development or production
optimizations.

```npm run server-none```
Runs a webpack-dev-server without any mode specification, allowing for testing
under different configurations.

## Contributing

Contributions are welcome. Please read our contributing guidelines and submit
pull requests to our repository.

## Donations

Your support is appreciated! You can make a donation using the following
methods:

### Stripe

For donations via credit or debit card, please visit our Stripe donation page:
[Donate with Stripe](https://donate.stripe.com/7sIaFa9DK18s8Ra8ww)

### PayPal

To donate using PayPal, please use the following link:
[Donate with PayPal](https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y)

### Cryptocurrency

We also accept donations in cryptocurrency. Please send your contributions to
our crypto account:

- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

## Contact

For any inquiries, please contact us
at [office@codev-it.at](mailto:office@codev-it.at).
