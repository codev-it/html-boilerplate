const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const WebpackBeforeBuildPlugin = require('before-build-webpack');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const syncDirectory = require('sync-directory');
const configLoader = require('./scripts/webpack/config-loader');
const {
  lookupPlaceholder,
  lookupPlaceholderMultiple
} = require('./scripts/webpack/placeholder');
const {
  buildWatchList,
  buildShellEvents
} = require('./scripts/webpack/builder');

// Load config
const {
  DIST,
  STATIC,
  SCSS,
  JS,
  JS_EXTERNALS,
  WATCH,
  SHELL,
  PURGE_CSS,
  PURGE_CSS_RERUN_AFTER_JS,
  PORT,
  PROXY
} = configLoader();

module.exports = (env, options) => {
  if (DIST === undefined) {
    throw new Error('Config "DIST" ist required');
  }

  // Variables
  const port = PORT || 80;
  const staticPath = lookupPlaceholder(STATIC) || false;
  const assetsPath = lookupPlaceholder(DIST) || '';
  const js = lookupPlaceholderMultiple(JS) || false;
  const scss = lookupPlaceholderMultiple(SCSS) || false;
  const watch = buildWatchList(WATCH) || {};
  const [shellJs, shellScss] = buildShellEvents(SHELL) || [];
  const purgeCssRerunAfterJs = !!PURGE_CSS && PURGE_CSS_RERUN_AFTER_JS;

  // Default config
  // noinspection JSValidateTypes
  const defaultConfig = {
    mode: options.mode,
    plugins: [
      new BrowserSyncPlugin({
        proxy: PROXY || 'localhost',
        port
      })
    ],
    watchOptions: {
      ignored: '**/node_modules'
    }
  };

  if (staticPath) {
    defaultConfig.devServer = {
      historyApiFallback: true,
      port
    };

    defaultConfig.plugins = [
      new HtmlWebPackPlugin({
        filename: !env.WEBPACK_SERVE ? '../index.html' : 'index.html',
        template: `${staticPath}/index.html`
      })
    ];
  }

  // File config
  const fileConfig = [
    {
      test: /\.(eot|ttf|otf|woff|woff2)$/,
      type: 'asset/resource',
      generator: {
        filename: !env.WEBPACK_SERVE ? '../font/[name][ext]' : undefined
      }
    },
    {
      test: /\.(svg|png|jpg|jpeg|gif|ico)$/,
      type: 'asset/resource',
      generator: {
        filename: !env.WEBPACK_SERVE ? '../img/[name][ext]' : undefined
      }
    }
  ];

  // JS config
  // noinspection JSValidateTypes
  const jsConfig = {
    ...defaultConfig,
    name: 'js',
    entry: js,
    output: {
      path: `${assetsPath}/js`,
      filename: '[name].js',
      clean: !!staticPath
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    externals: JS_EXTERNALS || {},
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: ['babel-loader', 'astroturf/loader']
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
        },
        ...fileConfig
      ]
    },
    plugins: [
      ...defaultConfig.plugins,
      new WebpackBeforeBuildPlugin((stats, callback) => {
        if (!env.WEBPACK_SERVE && staticPath) {
          syncDirectory(staticPath, assetsPath);
        }
        callback();
      }, undefined),
      new WebpackShellPluginNext(shellJs),
      new ExtraWatchWebpackPlugin(watch)
    ],
    optimization: { minimizer: [new TerserPlugin()] }
  };

  // SCSS config
  // noinspection JSValidateTypes
  const scssConfig = {
    ...defaultConfig,
    name: 'scss',
    entry: scss,
    output: {
      path: `${assetsPath}/css`
    },
    resolve: {
      extensions: ['.scss']
    },
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ]
        },
        ...fileConfig
      ]
    },
    plugins: [
      ...defaultConfig.plugins,
      new MiniCssExtractPlugin({ filename: '[name].css' }),
      new FixStyleOnlyEntriesPlugin(),
      new WebpackShellPluginNext(shellScss),
      new ExtraWatchWebpackPlugin({
        ...watch,
        files: [].concat(
          watch.files || [],
          purgeCssRerunAfterJs ? [`${assetsPath}/**/*.js`] : []
        )
      })
    ],
    optimization: {
      minimizer: [
        new CssMinimizerPlugin
      ]
    }
  };

  // Webpack config
  const webpackConfig = [];
  if (js) {
    webpackConfig.push(jsConfig);
  }
  if (scss) {
    webpackConfig.push(scssConfig);
  }
  return webpackConfig;
};
